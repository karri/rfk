#include <string.h>
#include <atari7800.h>
#include "maria.h"
#include "vblank.h"
#include "text.h"
#include "machinetype.h"


/* # of zones per screen area */
#define TOPZONES        3
#define DISPLAYZONES    SCREEN_HEIGHT
#define BOTZONES        4
#define NZONES          (TOPZONES + DISPLAYZONES + BOTZONES)

/* Start of each area in the display list list */
#define TOPSTART        0
#define DISPLAYSTART    TOPZONES
#define BOTSTART        (TOPZONES+DISPLAYZONES)


/* A line of text */
typedef struct {
    XHeader left;       /* Left half of text screen */
    XHeader right;      /* Right half of text screen */
    Header extra;       /* Extra character */
    NullHeader nh;
}TextZone;


extern const unsigned char font[];

#define NO_CONIO
#ifdef NO_CONIO
static DLLEntry dll[NZONES];
static TextZone zones[DISPLAYZONES];
static unsigned char screen[SCREEN_WIDTH * SCREEN_HEIGHT];
#else
extern DLLEntry dll[NZONES];
extern TextZone zones[DISPLAYZONES];
extern unsigned char screen[SCREEN_WIDTH * SCREEN_HEIGHT];
#endif


static const unsigned char colors[NCOLORS] = {
    0x74,
    0xb7,
    0x9a,
    0x34,
    0x49,
    0xf5,
    0x07,
    0x0f,
};

#ifdef NO_CONIO
static void initDLL(void) {

    unsigned char i;

    /* 25 additional scanlines for PAL */
    dll[0].offset = 0x80 | 15;
    dll[0].addrhi = ((unsigned int)&zones[0].nh) >> 8;
    dll[0].addrlo = ((unsigned int)&zones[0].nh) & 0xff;
    dll[1].offset = 0x80 | 8;
    dll[1].addrhi = ((unsigned int)&zones[0].nh) >> 8;
    dll[1].addrlo = ((unsigned int)&zones[0].nh) & 0xff;

    /* Top border, 9 scanlines */
    dll[2].offset = 0x80 | 8;
    dll[2].addrhi = ((unsigned int)&zones[0].nh) >> 8;
    dll[2].addrlo = ((unsigned int)&zones[0].nh) & 0xff;

    /* Display area */
    for (i=DISPLAYSTART; i<BOTSTART; i++) {
        dll[i].offset = 0x80 | CHAR_HEIGHT-1;
        dll[i].addrhi = ((unsigned int)&zones[i-DISPLAYSTART]) >> 8;
        dll[i].addrlo = ((unsigned int)&zones[i-DISPLAYSTART]) & 0xff;
    }

    /* Bottom border, 26 scanlines */
    dll[BOTSTART+0].offset = 0x80 | 15;
    dll[BOTSTART+0].addrhi = ((unsigned int)&zones[0].nh) >> 8;
    dll[BOTSTART+0].addrlo = ((unsigned int)&zones[0].nh) & 0xff;
    dll[BOTSTART+1].offset = 0x80 | 9;
    dll[BOTSTART+1].addrhi = ((unsigned int)&zones[0].nh) >> 8;
    dll[BOTSTART+1].addrlo = ((unsigned int)&zones[0].nh) & 0xff;

    /* 25 additional scanlines for PAL */
    dll[BOTSTART+2].offset = 0x80 | 15;
    dll[BOTSTART+2].addrhi = ((unsigned int)&zones[0].nh) >> 8;
    dll[BOTSTART+2].addrlo = ((unsigned int)&zones[0].nh) & 0xff;
    dll[BOTSTART+3].offset = 0x80 | 8;
    dll[BOTSTART+3].addrhi = ((unsigned int)&zones[0].nh) >> 8;
    dll[BOTSTART+3].addrlo = ((unsigned int)&zones[0].nh) & 0xff;
}


static void initZones(void) {

    unsigned char i;

    for (i=0; i<DISPLAYZONES; i++) {

        zones[i].left.addrlo = ((unsigned int)&screen[i*SCREEN_WIDTH]) & 0xff;
        zones[i].left.addrhi = ((unsigned int)&screen[i*SCREEN_WIDTH]) >> 8;
        zones[i].left.flags = 0x60;
        zones[i].left.palwidth = ((BLUE & 7)<<5) | ((-20) & 31);
        zones[i].left.hpos = 0;

        zones[i].right.addrlo = ((unsigned int)&screen[i*SCREEN_WIDTH+(SCREEN_WIDTH/2)]) & 0xff;
        zones[i].right.addrhi = ((unsigned int)&screen[i*SCREEN_WIDTH+(SCREEN_WIDTH/2)]) >> 8;
        zones[i].right.flags = 0x60;
        zones[i].right.palwidth = ((BLUE & 7)<<5) | ((-20) & 31);
        zones[i].right.hpos = 80;

        /*
         * Setting palwidth to zero makes extra to a nullheader.
         * addrhi never changes and can already be set.
         */
        zones[i].extra.addrlo = 0;
        zones[i].extra.palwidth = 0;
        zones[i].extra.addrhi = ((unsigned int)&font) >> 8;
        zones[i].extra.hpos = 0;

        zones[i].nh.unused = 0;
        zones[i].nh.zero = 0;
    }
}
#endif

void txtInit(void) {

    unsigned char i;

#ifdef NO_CONIO
    initDLL();
    initZones();
#endif

    txtClrScr();

    /* Set DPP, depending on machine type */
#ifdef NO_CONIO
    if (MT_NTSC == get_tv()) {
        DPPL = ((unsigned int) &dll[2]) & 0xff;
        DPPH = ((unsigned int) &dll[2]) >> 8;
    } else {
        DPPL = ((unsigned int) &dll[0]) & 0xff;
        DPPH = ((unsigned int) &dll[0]) >> 8;
    }

    /* Set up remaining maria registers karri */
    waitVBlank();
    CHBASE = ((unsigned int)&font[0]) >> 8;
    BKGRND = 0x00;
    CTRL = CTRL_MODEAC |
        CTRL_KANGOFF |
        CTRL_BCBLACK |
        CTRL_CHAR1B |
        CTRL_DMAON |
        CTRL_CKOFF;
#endif

    /* Set colors */
    for (i=0; i<NCOLORS; i++) {
        ((unsigned char*)&P0C2)[i*4] = colors[i];
    }
}


void txtClrScr(void) {

    unsigned char y;

    memset(screen, ' ', SCREEN_WIDTH*SCREEN_HEIGHT);

    for (y=0; y<SCREEN_HEIGHT; y++) {
        txtPutExtraChar(0, y, BLUE, ' ');
        txtSetRowColor(y, BLUE);
    }
}


void txtSetRowColor(unsigned char row, unsigned char color) {

    if (row >= SCREEN_HEIGHT) {
        return;
    }

    zones[row].left.palwidth = ((color & 7)<<5) | ((-20) & 31);
    zones[row].right.palwidth = ((color & 7)<<5) | ((-20) & 31);
}


unsigned char txtGetRowColor(unsigned char row) {

    if (row >= SCREEN_HEIGHT) {
        return 0;
    } else {
        return zones[row].left.palwidth >> 5;
    }
}


void txtPutChar(unsigned char x, unsigned char y, char character) {

    if ( (x >= SCREEN_WIDTH) || (y >= SCREEN_HEIGHT) ) {
        return;
    }

    screen[y*SCREEN_WIDTH+x] = character;
}


char txtReadChar(unsigned char x, unsigned char y) {

    if ( (x >= SCREEN_WIDTH) || (y >= SCREEN_HEIGHT) ) {
        return 0;
    } else {
        return screen[y*SCREEN_WIDTH+x];
    }
}


void txtPrintString(unsigned char x, unsigned char y, const char *s) {

    while (*s != 0) {

        if ('\n' == *s) {
            x = 0;
            y++;
        } else {
            txtPutChar(x, y, *s);
            x++;
            if (x >= SCREEN_WIDTH) {
                x = 0;
                y++;
            }
        }

        s++;
    }
}


void txtPrintStringCentered(unsigned char y, const char *s) {

    unsigned int len;

    len = strlen(s);

    if ( (len > SCREEN_WIDTH) || (y >= SCREEN_HEIGHT) ) {
        return;
    }

    txtPrintString((SCREEN_WIDTH-len)/2, y, s);
}


void txtPutExtraChar(unsigned char x, unsigned char y, unsigned char color, char character) {

    if ( (x >= SCREEN_WIDTH) || (y >= SCREEN_HEIGHT) ) {
        return;
    }

    zones[y].extra.hpos = x * (CHAR_WIDTH / 2);
    zones[y].extra.addrlo = character;
    zones[y].extra.palwidth = ((color & 7)<<5) | (-1 & 31);
}


unsigned char *txtGetScreen(void) {
    return screen;
}
