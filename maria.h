/*
 * Atari 7800 register definitions.
 * Copyright (C) 2004,2005 Thomas Mathys (killer@vantage.ch)
 *
 * This file is part of rfk7800.
 *
 * rfk7800 is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rfk7800 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rfk7800; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/*
 * Alternative TIA register definitions.
 */
#define INPTCTRL    TIA.inptctrl
#define PTCTRL      TIA.inptctrl
#define INPT0       TIA.inpt0
#define INPT1       TIA.inpt1
#define INPT2       TIA.inpt2
#define INPT3       TIA.inpt3
#define INPT4       TIA.inpt4
#define INPT5       TIA.inpt5
#define AUDC0       TIA.audc0
#define AUDC1       TIA.audc1
#define AUDF0       TIA.audf0
#define AUDF1       TIA.audf1
#define AUDV0       TIA.audv0
#define AUDV1       TIA.audv1

/*
 * Alternative RIOT register definitions.
 */
#define SWCHA       RIOT.swcha
#define CTLSWA      RIOT.ctlswa
#define SWCHB       RIOT.swchb
#define CTLSWB      RIOT.ctlswb
#define INTIM       RIOT.intim
#define TIM1T       RIOT.tim1t
#define TIM8T       RIOT.tim8t
#define TIM64T      RIOT.tim64t
#define T1024T      RIOT.t1024t

/*
 * Alternative MARIA register definitions.
 */
#define BKGRND      MARIA.bkgrnd
#define BACKGRND    MARIA.bkgrnd
#define P0C1        MARIA.p0c1
#define P0C2        MARIA.p0c2
#define P0C3        MARIA.p0c3
#define WSYNC       MARIA.wsync
#define P1C1        MARIA.p1c1
#define P1C2        MARIA.p1c2
#define P1C3        MARIA.p1c3
#define MSTAT       MARIA.mstat
#define P2C1        MARIA.p2c1
#define P2C2        MARIA.p2c2
#define P2C3        MARIA.p2c3
#define DPPH        MARIA.dpph
#define DPH         MARIA.dpph
#define P3C1        MARIA.p3c1
#define P3C2        MARIA.p3c2
#define P3C3        MARIA.p3c3
#define DPPL        MARIA.dppl
#define DPL         MARIA.dppl
#define P4C1        MARIA.p4c1
#define P4C2        MARIA.p4c2
#define P4C3        MARIA.p4c3
#define CHBASE      MARIA.chbase
#define P5C1        MARIA.p5c1
#define P5C2        MARIA.p5c2
#define P5C3        MARIA.p5c3
#define OFFSET      MARIA.offset
#define P6C1        MARIA.p6c1
#define P6C2        MARIA.p6c2
#define P6C3        MARIA.p6c3
#define CTRL        MARIA.ctrl
#define P7C1        MARIA.p7c1
#define P7C2        MARIA.p7c2
#define P7C3        MARIA.p7c3


/*
 * Display list entry
 */
typedef struct {
    unsigned char offset;
    unsigned char addrhi;
    unsigned char addrlo;
}DLLEntry;


/*
 * A null header.
 * The unused field can be used to store data.
 */
typedef struct {
    unsigned char unused;
    unsigned char zero;
}NullHeader;


/*
 * Normal (4 byte) header
 */
typedef struct {
    unsigned char addrlo;
    unsigned char palwidth;
    unsigned char addrhi;
    unsigned char hpos;
}Header;


/*
 * Extended (5 byte) header
 */
typedef struct {
    unsigned char addrlo;
    unsigned char flags;
    unsigned char addrhi;
    unsigned char palwidth;
    unsigned char hpos;
}XHeader;

/*
 * MSTAT register constants
 */
#define MSTAT_VBLANK    0x80    /* vblank is on when bit 7 is set */


/*
 * CTRL register constants
 */
#define CTRL_CKOFF      0x00    /* color kill on/off */
#define CTRL_CKON       0x80
#define CTRL_DMAOFF     0x60    /* dma on/off */
#define CTRL_DMAON      0x40
#define CTRL_CHAR1B     0x00    /* one/two byte characters */
#define CTRL_CHAR2B     0x10
#define CTRL_BCBLACK    0x00    /* black border */
#define CTRL_BCBKGRND   0x08    /* border has background color */
#define CTRL_KANGOFF    0x00    /* kangaroo mode on/off */
#define CTRL_KANGON     0x04
#define CTRL_MODE160    0x00    /* mode 160x2 or 160x4 */
#define CTRL_MODEBD     0x02    /* mode 320b or 320d */
#define CTRL_MODEAC     0x03    /* mode 320a or 320c */

