/*
 * Machine type detection.
 * Copyright (C) 2004,2005 Thomas Mathys (killer@vantage.ch)
 *
 * This file is part of rfk7800.
 *
 * rfk7800 is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rfk7800 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rfk7800; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
#ifndef MACHINETYPE_H
#define MACHINETYPE_H


#define MT_NTSC     0
#define MT_PAL      1
#define MT_UNKNOWN  255         /* internal use only */


/*
 * Detects machine type. The machine type is cached, so only
 * the first call to the functions performs an actual detection.
 * Return value : MT_NTSC or MT_PAL
 */
unsigned char getMachineType(void);


#endif
