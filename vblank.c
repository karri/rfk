/*
 * vblank wait routines.
 * Copyright (C) 2004,2005 Thomas Mathys (killer@vantage.ch)
 *
 * This file is part of rfk7800.
 *
 * rfk7800 is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rfk7800 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rfk7800; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include <atari7800.h>
#include "maria.h"
#include "vblank.h"


void waitVBlankOff(void) {
    while (MSTAT & MSTAT_VBLANK);
}


void waitVBlankOn(void) {
    while (!(MSTAT & MSTAT_VBLANK));
}


void waitVBlank(void) {
    waitVBlankOff();
    waitVBlankOn();
}
