/*
 * Keyboard input code
 * Copyright (C) 2004,2005 Thomas Mathys (killer@vantage.ch)
 *
 * This file is part of rfk7800.
 *
 * rfk7800 is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rfk7800 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rfk7800; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include <atari7800.h>
#include "maria.h"
#include "keys.h"
#include "vblank.h"


void keysInit(void) {
    RIOT.swacnt = 0;
    RIOT.swcha = 0xff;
    RIOT.swbcnt = 0x14;
    RIOT.swchb = ~0x14;
}


unsigned char keysRead(void) {
    unsigned char keys;
    keys = (~RIOT.swcha >> 1) & (K_UP | K_DOWN | K_LEFT | K_RIGHT);
    if ((TIA.inpt0 | TIA.inpt1) & 128) {
        keys |= K_FIRE;
    }
    return keys;
}


unsigned char getKey(void) {

    unsigned char k;

    while (1) {
        waitVBlank();
        k = keysRead();
        waitVBlank();
        if ( (k == keysRead()) && (k != K_NONE) ) {
            break;
        }
    }

    return k;
}


unsigned char keysWait(void) {
    while (keysRead() != K_NONE);
    return getKey();
}
