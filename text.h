/*
 * Text display for robotfindskitten.
 * Copyright (C) 2004,2005 Thomas Mathys (killer@vantage.ch)
 *
 * This file is part of rfk7800.
 *
 * rfk7800 is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rfk7800 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rfk7800; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *
 * 40x25 text display. To save memory, all characters on a single
 * line have the same color. Because it would suck if the robot
 * changed its color as it moved vertically over the screen, there
 * is an extra character per line of text which has its own color,
 * and which is usually used for the robot.
 */
#ifndef TEXT_H
#define TEXT_H

#define SCREEN_WIDTH    40
#define SCREEN_HEIGHT   28
#define CHAR_WIDTH      8
#define CHAR_HEIGHT     8


#define BLUE            0
#define GREEN           1
#define CYAN            2
#define RED             3
#define MAGENTA         4
#define BROWN           5
#define GREY            6
#define WHITE           7
#define NCOLORS         8


/* Initialize text screen */
void txtInit(void);


/* clear text screen */
void txtClrScr(void);


/*
 * Set the color for a row of text.
 * The function does nothing if an invalid row number is passed.
 */
void txtSetRowColor(unsigned char row, unsigned char color);


/*
 * Get the color of a row of text.
 * The function returns zero if an invalid row is passed
 */
unsigned char txtGetRowColor(unsigned char row);


/*
 * Draw a character (raw output \n and the likes aren't treated specially)
 * The function does nothing if invalid coordinates are passed.
 */
void txtPutChar(unsigned char x, unsigned char y, char character);


/*
 * Read characer at x/y.
 * Returns zero if invalid coordinates are passed.
 */
char txtReadChar(unsigned char x, unsigned char y);


/*
 * Draw an asciiz string.
 * The function starts drawing characters from (x,y).
 * If it reaches the end of the line or encounters a \n
 * It goes to the start of the next line.
 * If it reaches the end of the screen it stops drawing.
 */
void txtPrintString(unsigned char x, unsigned char y, const char *s);


/*
 * Draw an asciiz string centered on a line.
 * If y is invalid or the string is too long the function does nothing
 */
void txtPrintStringCentered(unsigned char y, const char *s);


/*
 * Draw extra character.
 * The function does nothing if invalid coordinates are passed.
 */
void txtPutExtraChar(unsigned char x, unsigned char y, unsigned char color, char character);


/*
 * Returns a pointer to the text screen buffer,
 * so that it can be manipulated directly.
 */
unsigned char *txtGetScreen(void);


#endif
