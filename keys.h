/*
 * Keyboard input code
 * Copyright (C) 2004,2005 Thomas Mathys (killer@vantage.ch)
 *
 * This file is part of rfk7800.
 *
 * rfk7800 is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rfk7800 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rfk7800; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
#ifndef KEYS_H
#define KEYS_H


enum {
    K_NONE = 0x00,
    K_UP = 0x08,
    K_DOWN = 0x10,
    K_LEFT = 0x20,
    K_RIGHT = 0x40,
    K_FIRE = 0x80,      /* Both fire buttons */
    K_ALL = K_UP | K_DOWN | K_LEFT | K_RIGHT | K_FIRE
};


/* Initialize registers */
void keysInit(void);


/*
 * Reads keys, return value is a combination of the K_xxx constants.
 */
unsigned char keysRead(void);


/*
 * Reads keys, return value is a combination of the K_xxx constants.
 * Keys are only reported if the staid the same during a frame.
 */
unsigned char getKey(void);


/*
 * Wait until all keys are released, then wait until keys
 * are pressed. Returns a combination of K_xxx constants.
 */
unsigned char keysWait(void);


#endif
