include Common.mk

objects= \
	keys.o \
	text.o \
	font.o \
	vblank.o \
	rfk7800.o \

target = rfk.a78

all: $(target)

$(target) : $(objects)
	$(CL) -t atari7800 -o $@ --force-import __EXEHDR__ -m rfk.map $(objects) -l atari7800
	sign7800 -w $(target)

clean:
	$(RM) $(objects) $(target) rfk.map *.c~

