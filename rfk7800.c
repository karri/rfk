#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <atari7800.h>

#include "maria.h"
#include "text.h"
#include "vblank.h"
#include "messages.h"
#include "machinetype.h"
#include "keys.h"
#include "version.h"

enum {
    MAX_BOGUS = 20,
    MAX_OBJECTS = MAX_BOGUS + 2,
    MENU_X = 14,
    MENU_Y = 15,
    MENU_SPACING = 2,
    STATUSBAR_YOFS = 0,
    STATUSBAR_HEIGHT = NKI_MESSAGES_MAXLINES,
    PLAYFIELD_WIDTH = SCREEN_WIDTH,
    PLAYFIELD_HEIGHT = SCREEN_HEIGHT - STATUSBAR_HEIGHT - 1,
    PLAYFIELD_XOFS = 0,
    PLAYFIELD_YOFS = STATUSBAR_HEIGHT + 1,
};


enum {
    ITEM_FINDKITTEN = 0,
    ITEM_INSTRUCTIONS,
    ITEM_ABOUT,
    NMENUITEMS,
};


enum {
    ROBOT = 0,
    KITTEN = 1,
    BOGUS = 2,
};


typedef struct {
    unsigned char x;
    unsigned char y;
    unsigned char character;
    unsigned int bogusmsg;  /* Index of bogus message */
}ScreenObject;


static unsigned char num_bogus;
static const char *last_bogus_message;
static ScreenObject objects[MAX_OBJECTS];


/* Macros for generating random numbers in different ranges */
#define randx()     (rand() % PLAYFIELD_WIDTH)
#define randy()     (rand() % PLAYFIELD_HEIGHT)
#define randchar()  (rand() & 255)
#define randmsg()   (rand() % NKI_MESSAGES_N)


static const char * const robot_image[] = {
    "[-]",
    "(+)=C",
    "| |",
    "OOO",
};
static const char * const kitten_image[] = {
    "|\\_/|",
    "|o o|__",
    "=-*-=__\\",
    "C_C_(___)",
};
static const char * const heart_image[] = {
    ".::. .::.",
    ":::::::::",
    "\140:::::::'",
    "  \140:::'",
};
static const char * const menuitems[] = {
    "findkitten!",
    "Instructions",
    "About",
};
static const char * const instructions[] = {
    "robotfindskitten instructions",
    "-----------------------------",
    "",
    "",
    "In this game, you are robot (#).",
    "Your job is to find kitten.",
    "",
    "This task is complicated by the",
    "existence of various things which",
    "are not kitten.",
    "",
    "Robot must touch items to determine",
    "if they are kitten or not.",
    "",
    "The game ends when robotfindskitten.",
    "",
    "While playing, use joystick to move",
    "robot, fire button to abort game.",
    "",
    "",
    "Press fire button to continue.",
};


static unsigned char validchar(char a) {

    switch (a) {
        case '#':
        case ' ':
        case 0:
        case 255:
            return 0;
            break;
        default:
            return 1;
            break;
    }
}


static void drawObject(const ScreenObject *obj) {

    unsigned char x,y;

    x = PLAYFIELD_XOFS + obj->x;
    y = PLAYFIELD_YOFS + obj->y;

    if ('#' == obj->character) {
        txtPutExtraChar(x, y, GREY, '#');
    } else {
        txtPutChar(x, y, obj->character);
    }
}


/*
 * Render a bunch of text lines.
 * x,y      :   Upper left corner of first line.
 *              Remaining lines are drawn below (same x)
 * color    :   Text color
 * count    :   # of lines to draw
 * lines    :   Pointers to the lines to draw (each zero terminated)
 */
static void printLines(unsigned char x, unsigned char y,
    unsigned char color, unsigned char count,
    const char * const lines[]) {

    unsigned char i;
    for (i=0; i<count; ++i) {
        txtSetRowColor(y+i, color);
        txtPrintString(x, y+i, lines[i]);
    }
}


/*
 * (Re)draw main menu
 * redraw   :   nonzero = redraw whole menu
 *              zero = redraw cursor only
 * selItem  :   currently selected item
 */
static void drawMainMenu(unsigned char redraw, unsigned char selItem) {

    unsigned char i;
    char buf[SCREEN_WIDTH + 1];

    if (redraw) {
        txtClrScr();
        txtSetRowColor(0, BLUE);
        txtSetRowColor(27, BLUE);
        txtPrintString(0, 0, "========================================");
        txtPrintString(0, 27, "========================================");
        printLines(15, 1, RED, sizeof(heart_image)/sizeof(heart_image[0]), heart_image);
        printLines(8, 6, GREY, sizeof(robot_image)/sizeof(robot_image[0]), robot_image);
        printLines(27, 6, GREY, sizeof(kitten_image)/sizeof(kitten_image[0]), kitten_image);

        txtSetRowColor(11, WHITE);
        txtPrintStringCentered(11, "robotfindskitten 7800");

        for (i=0; i<NMENUITEMS; i++) {
            txtSetRowColor(MENU_Y+i*MENU_SPACING, CYAN);
            txtPrintString(MENU_X, MENU_Y+i*MENU_SPACING, menuitems[i]);
        }

        txtSetRowColor(26, GREY);
        strcpy(buf, "v");
        strcat(buf, VERSION);
        strcat(buf, " on ");
        strcat(buf, get_tv() == MT_NTSC ? "an NTSC" : "a PAL");
        strcat(buf, " 7800");
        txtPrintStringCentered(26, buf);
    }

    /* Redraw cursor */
    for (i=0; i<NMENUITEMS; i++) {
        txtPutExtraChar(MENU_X-2, MENU_Y+i*MENU_SPACING, GREY, ' ');
    }
    txtPutExtraChar(MENU_X-2, MENU_Y+selItem*MENU_SPACING, GREY, '#');
}


/* Display instructions and wait until a fire button is pressed */
static void showInstructions() {

    enum {
        X_OFFSET = 2,
        Y_OFFSET = 3,
    };

    unsigned char y;

    txtClrScr();

    for (y=0; y<sizeof(instructions)/sizeof(instructions[0]); y++) {
        txtSetRowColor(Y_OFFSET+y, GREY);
        txtPrintString(X_OFFSET, Y_OFFSET+y, instructions[y]);
    }

    while (K_FIRE != keysWait()) {}
}


/* Display about screen and wait until a fire button is pressed */
static void showPointlessInformation() {

    typedef struct {
        unsigned char color;
        char * const text;
    }InfoLine;

    static const InfoLine infolines[] = {
        {WHITE, "robotfindskitten"},
        {GREY, ""},
        {GREY, "Compiled by Karri on the new cc65"},
        {GREY, "that supports atari7800"},
        {GREY, ""},
        {GREY, "By the illustrious Leonard Richardson"},
        {GREY, "Copyright (C) 1997, 2000"},
        {GREY, ""},
        {GREY, "Written originally for the"},
        {GREY, "Nerth Pork robotfindskitten contest."},
        {GREY, ""},
        {GREY, "http://robotfindskitten.org"},
        {GREY, ""},
        {WHITE, "robotfindskitten 7800"},
        {GREY, ""},
        {GREY, "Copyright (C) 2004, 2005 Thomas Mathys"},
        {GREY, "http://rfk7800.sourceforge.net"},
        {GREY, ""},
        {GREY, "This version has been exclusively"},
        {GREY, "prepared for the"},
        {GREY, "2004/2005 7800 Homebrewerpalooza Contest"},
        {GREY, ""},
        {GREY, ""},
        {GREY, "Press fire button to continue."},
    };

    enum {
        YOFFSET = 0,
    };

    unsigned char i;

    txtClrScr();

    for (i=0; i<sizeof(infolines)/sizeof(infolines[0]); ++i) {
        txtSetRowColor(YOFFSET + i, infolines[i].color);
        txtPrintStringCentered(YOFFSET + i, infolines[i].text);
    }

    while (K_FIRE != keysWait()) {}
}


/*
 * Uses the screen buffer to determine wether every
 * object has a unique position, so the screen should
 * be cleared first before this function is called
 * (initScreen does this)
 */
static void initObjects(void) {

    unsigned char i,j,ok;
    unsigned int msg;

    /*
     * Robot is handled separately, because its drawn
     * using the extra character, which is not stored
     * in the normal screen buffer.
     */
    objects[ROBOT].x = randx();
    objects[ROBOT].y = randy();
    objects[ROBOT].character = '#';
    objects[ROBOT].bogusmsg = 0;
    drawObject(&objects[ROBOT]);

    for (i=1; i<num_bogus+2; i++) {

        /* Create unique position */
        do {
            objects[i].x = randx();
            objects[i].y = randy();
        } while ( (' ' != txtReadChar(PLAYFIELD_XOFS+objects[i].x, PLAYFIELD_YOFS+objects[i].y)) ||
                ((objects[0].x == objects[i].x) &&
                (objects[0].y == objects[i].y)) );

        /* Character */
        do {
            objects[i].character = randchar();
        } while (!validchar(objects[i].character));

        /*
         * Select unique message for the object
         * actually the kitten gets a message too.
         * this is pretty silly, but who cares.
         */
        do {
            ok = 1;
            msg = randmsg();
            for (j=1; j<i; j++) {
                if (msg == objects[j].bogusmsg) {
                    ok = 0;
                    break;
                }
            }
        } while (!ok);
        objects[i].bogusmsg = msg;

        drawObject(&objects[i]);
    }
}


static void initScreen(void) {

    unsigned char y;

    txtClrScr();

    /* Status bar */
    for (y=0; y<=STATUSBAR_HEIGHT; y++) {
        txtSetRowColor(y, WHITE);
    }
    memset(txtGetScreen()+SCREEN_WIDTH*STATUSBAR_HEIGHT, 196, SCREEN_WIDTH);

    /* Choose random colors for the playfield lines */
    for (y=STATUSBAR_HEIGHT+1; y<SCREEN_HEIGHT; y++) {
        txtSetRowColor(y, rand());
    }
}


static void victoryAnimation(void) {

    enum {
        ROBOTY = SCREEN_HEIGHT / 2,
    };

    unsigned char kittencolor;
    unsigned char i,heartsvisible;
    signed char x;

    kittencolor = txtGetRowColor(PLAYFIELD_YOFS + objects[KITTEN].y);

    txtClrScr();

    txtSetRowColor(ROBOTY, kittencolor);

    /* Run towards eachother */
    for (x=9; x>=0; x--) {

        /* Erase old kitten */
        txtPutChar(SCREEN_WIDTH/2+x+1, ROBOTY, ' ');

        /* Draw new robot/kitten */
        txtPutExtraChar(SCREEN_WIDTH/2-1-x, ROBOTY, GREY, objects[ROBOT].character);
        txtPutChar(SCREEN_WIDTH/2+x, ROBOTY, objects[KITTEN].character);

        /* Delay animation a bit */
        i = MT_NTSC == get_tv() ? 6 : 5;
        for (; i!=0; i--) {
            waitVBlank();
        }
    }

    txtSetRowColor(ROBOTY-4, WHITE);
    txtPrintStringCentered(ROBOTY-4, "You found kitten!");
    txtSetRowColor(ROBOTY+2, WHITE);
    txtPrintStringCentered(ROBOTY+2, "Way to go, robot!");
    txtSetRowColor(ROBOTY+8, GREY);
    txtPrintStringCentered(ROBOTY+8, "Press fire button to continue.");

    /* Flash hearts while waiting for the fire button */
    txtSetRowColor(ROBOTY-2, RED);
    heartsvisible = 1;
    while (1) {

        if (heartsvisible) {
            txtPrintStringCentered(ROBOTY-2, "\003\003");
        } else {
            txtPrintStringCentered(ROBOTY-2, "  ");
        }
        heartsvisible = !heartsvisible;

        /* Delay flashing a bit */
        i = MT_NTSC == get_tv() ? 20 : 17;
        for (; i!=0; i--) {
            waitVBlank();
            if (K_FIRE == keysRead()) {
                return;
            }
        }
    }
}


/* Returns the index of the item (from objects array)
 * That has the coordinates x/y.
 * return value :index of the item or 255 if item not found
 */
static unsigned char findItem(unsigned char x, unsigned char y) {

    unsigned char i;

    for (i=0; i<num_bogus+2; i++) {
        if ( (objects[i].x == x) && (objects[i].y == y) ) {
            return i;
        }
    }

    return -1;
}


/*
 * print bogus message in the status bar.
 * message  :   pointer to the message to print.
 */
static void printBogusMessage(const char *message) {

    if (last_bogus_message != message) {
        last_bogus_message = message;
        memset(txtGetScreen(), ' ', SCREEN_WIDTH * STATUSBAR_HEIGHT);
        txtPrintString(0, 0, message);
    }
}


/*
 * Handle user input during the actual game
 * key : pressed key(s)
 * return value : zero = game continues
 *                nonzero = game over (won, lost or aborted)
 */
static unsigned char processInput(unsigned char key) {

    unsigned char check_x;
    unsigned char check_y;

    check_x = objects[ROBOT].x;
    check_y = objects[ROBOT].y;

    switch (key) {
        case K_FIRE:
            /* select : abort game */
            return 1;
            break;
        case K_LEFT:
            --check_x;
            break;
        case K_RIGHT:
            ++check_x;
            break;
        case K_UP:
            --check_y;
            break;
        case K_DOWN:
            ++check_y;
            break;
        case K_LEFT|K_UP:
            --check_x;
            --check_y;
            break;
        case K_LEFT|K_DOWN:
            --check_x;
            ++check_y;
            break;
        case K_RIGHT|K_UP:
            ++check_x;
            --check_y;
            break;
        case K_RIGHT|K_DOWN:
            ++check_x;
            ++check_y;
            break;

    }

    /*
     * Ccheck wether robot tries to findkitten offscreen.
     * Since we're working with unsigned coordinates we
     * Don't have to check for < 0
     */
    if ( (check_x >= PLAYFIELD_WIDTH) || (check_y >= PLAYFIELD_HEIGHT) ) {
        return 0;
    }

    /* Check for collision with kitten or non-kitten item */
    if (' ' != txtReadChar(PLAYFIELD_XOFS+check_x, PLAYFIELD_YOFS+check_y)) {
        unsigned char item = findItem(check_x, check_y);
        switch (item) {
            case ROBOT:
                /*
                 * We didn't move, or we're stuck in a
                 * time warp or something.
                 */
                break;
            case KITTEN:
                // Found it!
                victoryAnimation();
                return 1;
                break;
            case 255:
                /* this is an error. */
                printBogusMessage("Unholy error: invalid object index.");
                break;
            default:
                /* We hit a bogus object; print its message. */
                printBogusMessage(NKI_MESSAGES[objects[item].bogusmsg]);
                break;
        }
        return 0;
    }

    /* No collision, move robot */
    objects[ROBOT].x = check_x;
    objects[ROBOT].y = check_y;
    return 0;
}


/* Main game loop */
static void game(void) {

    unsigned char old_x;
    unsigned char old_y;
    unsigned char key;
    unsigned char i;
    extern unsigned char zonecounter;

    srand(rand() + zonecounter);

    num_bogus = MAX_BOGUS;
    last_bogus_message = NULL;

    initScreen();
    initObjects();
    txtPrintString(0, 0, "findkitten!");

    old_x = objects[ROBOT].x;
    old_y = objects[ROBOT].y;
    while (1) {

        key = getKey();

        if (processInput(key)) {
            return;
        }

        /* Redraw robot, where applicable. We're your station, robot. */
        if ( (old_x != objects[ROBOT].x) || (old_y != objects[ROBOT].y) ) {
            /* Get rid of the old robot */
            txtPutExtraChar(PLAYFIELD_XOFS+old_x, PLAYFIELD_YOFS+old_y, GREY, ' ');
            /* Meet the new robot, same as the old robot. */
            drawObject(&objects[ROBOT]);
            old_x = objects[ROBOT].x;
            old_y = objects[ROBOT].y;
        }

        for (i=0; i<4; i++) {
            waitVBlank();
        }
    }
}

unsigned char ch = 0;
char *dummy = "";

void main(void) {

    unsigned char item;

    keysInit();
    txtInit();

    item = 0;
    drawMainMenu(1, item);
    while (1) {
        switch (keysWait()) {
            case K_DOWN:
                if (item < NMENUITEMS-1) {
                    ++item;
                } else {
                    item = 0;
                }
                drawMainMenu(0, item);
                break;
            case K_UP:
                if (item > 0) {
                    --item;
                } else {
                    item = NMENUITEMS-1;
                }
                drawMainMenu(0, item);
                break;
            case K_FIRE:
                switch (item) {
                    case ITEM_FINDKITTEN:
                        game();
                        drawMainMenu(1, item);
                        break;
                    case ITEM_INSTRUCTIONS:
                        showInstructions();
                        drawMainMenu(1, item);
                        break;
                    case ITEM_ABOUT:
                        showPointlessInformation();
                        drawMainMenu(1, item);
                        break;
                }
                break;
        }
    }
}
