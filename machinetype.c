/*
 * Machine type detection.
 * Copyright (C) 2004,2005 Thomas Mathys (killer@vantage.ch)
 *
 * This file is part of rfk7800.
 *
 * rfk7800 is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rfk7800 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rfk7800; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include <atari7800.h>
#include "maria.h"
#include "vblank.h"
#include "machinetype.h"
#include <time.h>



#if 1
#define MSTAT_VBLANK    0x80    /* vblank is on when bit 7 is set */
static unsigned char machineType = MT_UNKNOWN;

unsigned char getMachineType2(void) {

    unsigned char n;

    if (MT_UNKNOWN == machineType) {

        waitVBlank();
        waitVBlankOff();

        n = 0;
        while (!(MARIA.mstat & MSTAT_VBLANK)) {
            MARIA.wsync = 0;
            MARIA.wsync = 0;
            --n;
        }

        machineType = n >= 0x78 ? MT_NTSC : MT_PAL;
    }

    return machineType;
}
#else

extern unsigned char get_tv();

unsigned char getMachineType2(void) {

    if (get_tv()) return MT_PAL;
    return MT_NTSC;
}
#endif

